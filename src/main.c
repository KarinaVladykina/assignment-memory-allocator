#include "mem_internals.h"
#include "mem.h"
#include "util.h"

static struct block_header *get_block_header(void *data);

bool test1() {
    printf("Тест 1 -  обычное выделение памяти ");

    void* malloc = _malloc(1000);
	if (malloc) {
        printf("\n Тест 1 завешен, памяти успешно выделена ");
        return true;
    }

  	printf("Тест 1 завершён - память успешно выделена.\n");
  	_free(malloc);
    return false;
}

bool test2() {
    printf("Тест 2 - Освобождение одного блока из нескольких выделенных\n");
    void* malloc1 = _malloc(100);
    void* malloc2 = _malloc(200);
    debug_heap(stdout, memory_block);
    size_t block_number1 = get_number_of_blocks(memory_block);
    _free(malloc2);
    debug_heap(stdout, memory_block);
    size_t block_number2 = get_number_of_blocks(memory_block);
    _free(malloc1);
    if(block_number1 == 2 && block_number2 == 1) {
        printf("\nОсвобождение одного блока из нескольких выделенных произошло успешно\n");
    } else {
        printf("\n:Освобождение одного блока из нескольких выделенных не удалось\n");
        return false;
    }
}

bool test3(){ 
    printf("Тест 3 - Освобождение двух блоков из нескольких выделенных.\n");

    void* malloc1 = _malloc(150);
	void* malloc2 = _malloc(150);
  	void* malloc3 = _malloc(150);

    if (!malloc1 || !malloc2 || !malloc3){
        printf("Ошибка- пустой блок памяти.\n");
        return false;
    }

    _free(malloc3);
	_free(malloc2);
    if (!get_block_header(malloc1)->is_free || !get_block_header(malloc2)->is_free){
        printf("Ошибка - блоки памяти не освобождены.\n");
        return false;
    }

    _free(malloc1);
    printf("Несколько блоков успешно освобождены.\n");
	return true;
}

bool test_4(){ 
    printf("Тест 4 - Память закончилась, новый регион памяти расширяет старый.\n");

	void* malloc = _malloc(1050);
	if (malloc != ((int8_t*)HEAP_START) + offsetof(struct block_header, contents)) {
		printf("регион памяти не увеличился.");
		return false;
	}

    _free(malloc);
	printf(" новый регион памяти успешно расширил старый.\n");
	return true;

}

bool test_5(){ 
    printf(" Память закончилась, выделение памяти в другом месте.\n");
	
    void* malloc1 = _malloc(10000);
    void* malloc2 = _malloc(15000);

    size_t block_number1 = get_block_header(malloc1);
    size_t block_number2 = get_block_header(malloc2);

    mmap(block_number2->next, 300000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	void* a_malloc = _malloc(45000);
    
	if (block_number2 == (uint8_t*) a_malloc - offsetof(struct block_header, contents)) {
        printf("Ошибка - новый регион выделен за счёт старого.");
		return false;
	}

	_free(a_malloc);
	printf("Новый регион памяти успешно выделен в другом месте.\n");
	return true;
}

static struct block_header *get_block_header(void *data) {
    return (struct block_header*)((uint8_t*)data - offsetof(struct block_header, contents));
}

int main(int argc, char** argv){
    if (test1() && test2() && test3() && test4() && test5()) printf("Все тесты пройдены успешно!");
    else printf("Один или более тестов не были выполнены успешно.");

    return 0;
}



